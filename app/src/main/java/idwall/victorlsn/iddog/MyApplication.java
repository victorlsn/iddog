package idwall.victorlsn.iddog;

import android.app.Application;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import idwall.victorlsn.iddog.broadcast.NetworkReceiver;

/**
 * Created by Victor on 26/04/2018.
 */

public class MyApplication extends Application {
    private static MyApplication instance;


    public MyApplication() {
        instance = this;
    }

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(new NetworkReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
}
