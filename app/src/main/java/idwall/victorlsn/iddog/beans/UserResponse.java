package idwall.victorlsn.iddog.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Victor on 24/04/2018.
 */

public class UserResponse implements Serializable {
    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
