package idwall.victorlsn.iddog.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by victorlsn on 25/04/18.
 */

public class ErrorResponse implements Serializable {
    private APIError error;

    public APIError getError() {
        return error;
    }

    public void setError(APIError error) {
        this.error = error;
    }
}
