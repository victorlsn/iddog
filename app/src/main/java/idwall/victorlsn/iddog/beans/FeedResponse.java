package idwall.victorlsn.iddog.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by victorlsn on 25/04/18.
 */

public class FeedResponse implements Serializable {
    private String category;
    @SerializedName("list")
    private List<String> images;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
