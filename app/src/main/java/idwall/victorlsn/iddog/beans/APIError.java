package idwall.victorlsn.iddog.beans;

import java.io.Serializable;

/**
 * Created by victorlsn on 25/04/18.
 */

public class APIError implements Serializable {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
