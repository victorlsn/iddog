package idwall.victorlsn.iddog.presenters;

import android.widget.Toast;

import idwall.victorlsn.iddog.interfaces.BaseMVP;
import idwall.victorlsn.iddog.interfaces.LoginMVP;
import idwall.victorlsn.iddog.models.LoginModelImp;
import idwall.victorlsn.iddog.util.AppTools;

/**
 * Created by Victor on 24/04/2018.
 */

public class LoginPresenterImp implements LoginMVP.Presenter {
    private LoginMVP.View  view;
    private LoginMVP.Model model;

    public LoginPresenterImp() {
        model = new LoginModelImp(this);
    }

    @Override
    public boolean attachView(BaseMVP.View view) {
        if(null == view) return false;
        this.view = (LoginMVP.View) view;

        return true;
    }

    @Override
    public void requestToken(final String email) {
        if (null == email) return;

        model.generateToken(email);
        view.showProgressBar(true);
    }

    @Override
    public void requestTokenSuccessfully(String token) {
        view.showProgressBar(false);
        view.receiveTokenSuccessfully(token);
    }

    @Override
    public void requestTokenFailure(String message) {
        view.showProgressBar(false);
        if(null == message || message.isEmpty()) return;
        view.showToast(message, Toast.LENGTH_SHORT);
    }
}
