package idwall.victorlsn.iddog.presenters;

import android.widget.Toast;

import idwall.victorlsn.iddog.beans.FeedResponse;
import idwall.victorlsn.iddog.interfaces.BaseMVP;
import idwall.victorlsn.iddog.interfaces.FeedMVP;
import idwall.victorlsn.iddog.models.FeedModelImp;

/**
 * Created by Victor on 24/04/2018.
 */

public class FeedPresenterImp implements FeedMVP.Presenter {
    private FeedMVP.View  view;
    private FeedMVP.Model model;

    public FeedPresenterImp() {
        model = new FeedModelImp(this);
    }

    @Override
    public boolean attachView(BaseMVP.View view) {
        if(null == view) return false;
        this.view = (FeedMVP.View) view;

        return true;
    }

    @Override
    public void requestFeed(final String token, final String category) {
        if (null == token) return;

        view.showProgressBar(true);
        model.getFeed(token, category);
    }

    @Override
    public void requestFeedSuccessfully(FeedResponse feed) {
        view.showProgressBar(false);
        view.receiveFeedSuccessfully(feed);
    }

    @Override
    public void requestFeedFailure(String message) {
        view.showProgressBar(false);
        if(null == message || message.isEmpty()) return;
        view.showToast(message, Toast.LENGTH_SHORT);
    }
}
