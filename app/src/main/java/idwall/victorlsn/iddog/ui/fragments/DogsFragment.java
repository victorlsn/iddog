package idwall.victorlsn.iddog.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import idwall.victorlsn.iddog.R;
import idwall.victorlsn.iddog.beans.FeedResponse;
import idwall.victorlsn.iddog.event.NetworkConnectedEvent;
import idwall.victorlsn.iddog.interfaces.FeedMVP;
import idwall.victorlsn.iddog.presenters.FeedPresenterImp;
import idwall.victorlsn.iddog.ui.activities.ImageViewerActivity;
import idwall.victorlsn.iddog.ui.adapters.ImageAdapter;
import idwall.victorlsn.iddog.util.AppTools;

/**
 * Created by victorlsn on 25/04/18.
 */

public class DogsFragment extends BaseFragment implements FeedMVP.View{
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private FeedMVP.Presenter presenter;
    private ProgressDialog progressDialog;
    private String category;
    private String token;
    private ImageAdapter adapter;
    List<String> images = new ArrayList<>();
    private boolean isViewShown = false;
    private boolean isVisibleToUser = false;


    @Override
    protected int layoutToInflate() {
        return R.layout.dogs_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            category = bundle.getString(getString(R.string.category), "");
            token = bundle.getString(getString(R.string.token), "");
        }

        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView();
        configAdapter(images);
        initPresenter();

        if (!isViewShown) {
            presenter.requestFeed(token, category);
        }
    }

    @Subscribe
    public void onEvent(NetworkConnectedEvent event) {
        if (getView() != null) {
            isViewShown = true;
            initPresenter();
            if (isVisibleToUser) {
                if (images.size() == 0) {
                    presenter.requestFeed(token, category);
                }
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;

        if (getView() != null) {
            isViewShown = true;
            initPresenter();

            if (isVisibleToUser) {
                if (images.size() == 0) {
                    presenter.requestFeed(token, category);
                }
            }
        }
    }

    private void initPresenter() {
        if (null == presenter) {
            presenter = new FeedPresenterImp();
            presenter.attachView(this);
        }
    }

    private void initRecyclerView(){
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), AppTools.getGridSpanCount(getActivity()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
    }

    private void configAdapter(List<String> images){
        adapter = new ImageAdapter( getActivity(), images);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ImageAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(View view, String url) {
                Intent intent = new Intent(getActivity(), ImageViewerActivity.class);
                intent.putExtra("imageUrl", url);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(getActivity(), view, "TRANSITION");
                ActivityCompat.startActivity(getActivity(), intent, options.toBundle());            }
        });
    }

    @Override
    public void showProgressBar(boolean show) {
        if(show){
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Aguarde");
            progressDialog.setMessage("Buscando feed...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }else {
            if(null != progressDialog && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void showToast(String message, int duration) {
        AppTools.showToast(getActivity(), message, duration);
    }

    @Override
    public void receiveFeedSuccessfully(FeedResponse feed) {
        if (recyclerView != null) {
            configAdapter(feed.getImages());
        }
        images = feed.getImages();
    }
}
