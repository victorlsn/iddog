package idwall.victorlsn.iddog.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import idwall.victorlsn.iddog.R;
import idwall.victorlsn.iddog.interfaces.LoginMVP;
import idwall.victorlsn.iddog.presenters.LoginPresenterImp;
import idwall.victorlsn.iddog.util.AppTools;

/**
 * Created by Victor on 24/04/2018.
 */

public class SignUpActivity extends BaseActivity implements LoginMVP.View {
    private ProgressDialog progressDialog;
    private LoginMVP.Presenter presenter;
    private Animation scale;


    @BindView(R.id.login_et)
    EditText loginEditText;
    @BindView(R.id.login_bt)
    Button loginButton;

    @OnClick(R.id.login_bt)
    public void onLoginClick() {
        loginButton.startAnimation(scale);
        if (loginEditText.getText().toString().isEmpty()) {
            showToast(getString(R.string.email_toast), Toast.LENGTH_SHORT);
            return;
        }
        presenter.requestToken(loginEditText.getText().toString());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        scale = AnimationUtils.loadAnimation(this, R.anim.animation_button_scale);

        if(null == presenter){
            presenter = new LoginPresenterImp();
            presenter.attachView(this);
        }

        String token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(getString(R.string.token), null);
        if (token != null) {
            navigateToFeed(token);
        }
    }

    private void navigateToFeed(String token) {
        Intent intent = new Intent(this, FeedActivity.class);
        intent.putExtra(getString(R.string.token), token);
        startActivity(intent);
        finish();
    }
    @Override
    public void showProgressBar(boolean show) {
        if(show){
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Aguarde");
            progressDialog.setMessage("Efetuando login...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }else {
            if(null != progressDialog && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void showToast(String message, int duration) {
        AppTools.showToast(this, message, duration);
    }

    @Override
    public void receiveTokenSuccessfully(String token) {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(getString(R.string.token), token).apply();
        navigateToFeed(token);
    }
}
