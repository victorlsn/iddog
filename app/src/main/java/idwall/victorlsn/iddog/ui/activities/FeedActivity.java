package idwall.victorlsn.iddog.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import idwall.victorlsn.iddog.R;
import idwall.victorlsn.iddog.ui.adapters.PageFragmentAdapter;
import idwall.victorlsn.iddog.ui.fragments.DogsFragment;

/**
 * Created by Victor on 24/04/2018.
 */

public class FeedActivity extends BaseActivity {
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.toolbar_viewpager)
    Toolbar toolbar;


    private android.support.v7.app.ActionBar actionBar;
    private DogsFragment huskyFragment;
    private DogsFragment labradorFragment;
    private DogsFragment houndFragment;
    private DogsFragment pugFragment;
    private String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_activity);

        token = getIntent().getStringExtra(getString(R.string.token));

        prepareActionBar();
        setupFragments();
        setupViewPager();
        setupTabLayout();
        setupTabClick();
        actionBar.setTitle("THE IDDOG");
    }

    private void prepareActionBar() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
    }

    private void setupFragments() {
        if (huskyFragment == null) huskyFragment = new DogsFragment();
        if (labradorFragment == null) labradorFragment = new DogsFragment();
        if (houndFragment == null) houndFragment = new DogsFragment();
        if (pugFragment == null) pugFragment = new DogsFragment();

        Bundle husky = new Bundle();
        husky.putString(getString(R.string.category), "husky");
        husky.putString(getString(R.string.token), token);
        huskyFragment.setArguments(husky);

        Bundle labrador = new Bundle();
        labrador.putString(getString(R.string.category), "labrador");
        labrador.putString(getString(R.string.token), token);
        labradorFragment.setArguments(labrador);

        Bundle hound = new Bundle();
        hound.putString(getString(R.string.category), "hound");
        hound.putString(getString(R.string.token), token);
        houndFragment.setArguments(hound);

        Bundle pug = new Bundle();
        pug.putString(getString(R.string.category), "pug");
        pug.putString(getString(R.string.token), token);
        pugFragment.setArguments(pug);
    }

    /**
     * Método responsável por configurar o view pager com as fragments.
     */
    private void setupViewPager() {
        PageFragmentAdapter adapter = new PageFragmentAdapter(getSupportFragmentManager());

        adapter.addFragment(huskyFragment, "Husky");
        adapter.addFragment(labradorFragment, "Labrador");
        adapter.addFragment(houndFragment, "Hound");
        adapter.addFragment(pugFragment, "Pug");
        viewPager.setAdapter(adapter);
    }

    /**
     * Método responsável por configurar os icones na tab do view pager.
     */
    private void setupTabLayout() {
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.ic_husky));
        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.ic_labrador));
        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.ic_hound));
        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.drawable.ic_pug));
        tabLayout.getTabAt(0).setText("HUSKY");
        tabLayout.getTabAt(1).setText("LABRADOR");
        tabLayout.getTabAt(2).setText("HOUND");
        tabLayout.getTabAt(3).setText("PUG");
    }

    private void setupTabClick() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                invalidateOptionsMenu();
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}
