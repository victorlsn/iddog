package idwall.victorlsn.iddog.interfaces;

/**
 * Created by Victor on 19/04/2018.
 */

public interface LoginMVP {
    interface Model{
        void generateToken(String email);
    }
    interface Presenter extends BaseMVP.Presenter{
        void requestToken(String email);
        void requestTokenSuccessfully(String token);
        void requestTokenFailure(String message);
    }
    interface View extends BaseMVP.View{
        void receiveTokenSuccessfully(String token);
    }
}
