package idwall.victorlsn.iddog.interfaces;

/**
 * Created by Victor on 19/04/2018.
 */

public interface BaseMVP {
    interface Presenter{
        boolean attachView(BaseMVP.View view);
    }

    interface View{
        void showProgressBar(boolean show);
        void showToast(String message, int duration);
    }
}
