package idwall.victorlsn.iddog.interfaces;

import idwall.victorlsn.iddog.beans.FeedResponse;
import idwall.victorlsn.iddog.beans.UserResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Victor on 19/04/2018.
 */

public interface AppRestEndPoint {

    @Headers("Content-Type: application/json")
    @POST("/signup")
    Call<UserResponse> userSignup(
            @Query("email") String email);

    @Headers("Content-Type: application/json")
    @GET("/feed")
    Call<FeedResponse> getFeed(
            @Header("Authorization") String token,
            @Query("category") String category);
}
