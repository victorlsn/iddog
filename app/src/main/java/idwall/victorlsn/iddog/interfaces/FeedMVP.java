package idwall.victorlsn.iddog.interfaces;

import idwall.victorlsn.iddog.beans.FeedResponse;

/**
 * Created by victorlsn on 25/04/18.
 */

public interface FeedMVP {
    interface Model{
        void getFeed(String token, String category);
    }
    interface Presenter extends BaseMVP.Presenter{
        void requestFeed(String token, String category);
        void requestFeedSuccessfully(FeedResponse feed);
        void requestFeedFailure(String message);
    }
    interface View extends BaseMVP.View{
        void receiveFeedSuccessfully(FeedResponse feed);
    }
}
