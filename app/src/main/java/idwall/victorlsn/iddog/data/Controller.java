package idwall.victorlsn.iddog.data;

import android.support.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import idwall.victorlsn.iddog.MyApplication;
import idwall.victorlsn.iddog.interfaces.AppRestEndPoint;
import idwall.victorlsn.iddog.util.AppTools;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Victor on 24/04/2018.
 */

public class Controller {

    private static Controller instance = null;
    private AppRestEndPoint apiCall;

    private Controller() {
        File cacheDir = MyApplication.getInstance().getApplicationContext().getCacheDir();

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient
                .Builder()
                .cache(new Cache(cacheDir, 10 * 1024 * 1024)) // 10 MB
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        Request request = chain.request();
                        if (AppTools.isOnline()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 31536000).build();
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 31536000).build();
                        }
                        return chain.proceed(request);
                    }
                }).connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS);
        OkHttpClient client = okHttpBuilder.build();

        String BASE_URL = "https://iddog-api.now.sh";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        apiCall = retrofit.create(AppRestEndPoint.class);
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }

        return instance;
    }

    public AppRestEndPoint doApiCall() {
        return apiCall;
    }
}
