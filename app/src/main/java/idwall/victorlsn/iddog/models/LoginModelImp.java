package idwall.victorlsn.iddog.models;

import android.util.Log;

import com.google.gson.Gson;

import idwall.victorlsn.iddog.R;
import idwall.victorlsn.iddog.beans.ErrorResponse;
import idwall.victorlsn.iddog.beans.UserResponse;
import idwall.victorlsn.iddog.data.Controller;
import idwall.victorlsn.iddog.interfaces.LoginMVP;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Victor on 19/04/2018.
 */

public class LoginModelImp implements LoginMVP.Model {

    private LoginMVP.Presenter presenter;

    public LoginModelImp(LoginMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void generateToken(String email) {
        Controller.getInstance().doApiCall().userSignup(email).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (null != response.body()) {
                    String token = response.body().getUser().getToken();
                    presenter.requestTokenSuccessfully(token);
                }
                else {
                    try {
                        Gson gson = new Gson();
                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        presenter.requestTokenFailure(errorResponse.getError().getMessage());
                    }
                    catch (Exception e) {
                        Log.e("Exception: ", e.getLocalizedMessage());
                        presenter.requestTokenFailure("Error requesting data. Please check your connection or try again later");
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                presenter.requestTokenFailure(t.getMessage());
            }
        });
    }
}
