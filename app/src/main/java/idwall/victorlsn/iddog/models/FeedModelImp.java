package idwall.victorlsn.iddog.models;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import idwall.victorlsn.iddog.beans.ErrorResponse;
import idwall.victorlsn.iddog.beans.FeedResponse;
import idwall.victorlsn.iddog.data.Controller;
import idwall.victorlsn.iddog.interfaces.FeedMVP;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Victor on 19/04/2018.
 */

public class FeedModelImp implements FeedMVP.Model {

    private FeedMVP.Presenter presenter;

    public FeedModelImp(FeedMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getFeed(final String token, final String category) {
        Controller.getInstance().doApiCall().getFeed(token, category).enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(@NonNull Call<FeedResponse> call, @NonNull Response<FeedResponse> response) {
                if (null != response.body()) {
                    FeedResponse feed = response.body();
                    presenter.requestFeedSuccessfully(feed);
                }
                else {
                    try {
                        Gson gson = new Gson();
                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        presenter.requestFeedFailure(errorResponse.getError().getMessage());
                    }
                    catch (Exception e) {
                        Log.e("Exception: ", e.getLocalizedMessage());
                        presenter.requestFeedFailure("Error requesting data. Please check your connection or try again later");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<FeedResponse> call, Throwable t) {
                presenter.requestFeedFailure(t.getMessage());
            }
        });
    }
}
